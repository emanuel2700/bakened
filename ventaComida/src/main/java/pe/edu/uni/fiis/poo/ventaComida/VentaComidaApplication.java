package pe.edu.uni.fiis.poo.ventaComida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentaComidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentaComidaApplication.class, args);
	}

}
