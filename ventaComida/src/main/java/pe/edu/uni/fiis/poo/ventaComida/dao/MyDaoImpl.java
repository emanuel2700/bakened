package pe.edu.uni.fiis.poo.ventaComida.dao;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.poo.ventaComida.dao.datasource.MyDatasource;
import pe.edu.uni.fiis.poo.ventaComida.domain.Cliente;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao{

    @Override
    public Cliente registrarCliente(Cliente request) {
        String sql = "insert into cliente (nombre, correo, dni, direccion, clave) " +
                "values (?, ?, ?, ?, ?)";
        this.jdbcTemplate.update(sql, new String[]{
                request.getNombre(),
                request.getCorreo(),
                request.getDni(),
                request.getDireccion(),
                request.getClave()
        });
        return null;
    }

    /*
    @Override
    public Usuario registrarUsuario(Usuario request) {

        String sql = "insert into usuario (dni, " +
                "nombre, \n" +
                "celular, \n" +
                "cod_confirmacion, \n" +
                "comprador, \n" +
                "repartidor, \n" +
                "distrito)" +
                "values (?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(sql, new String[]{
                request.getDni(),
                request.getNombre(),
                request.getCelular(),
                request.getCod_confirmacion(),
                request.getComprador(),
                request.getRepartidor(),
                request.getDistrito()
        });

        return request;
    }
*/

}
