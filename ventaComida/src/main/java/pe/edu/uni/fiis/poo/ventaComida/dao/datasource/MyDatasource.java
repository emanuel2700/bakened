package pe.edu.uni.fiis.poo.ventaComida.dao.datasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class MyDatasource {
    @Autowired
    protected JdbcTemplate jdbcTemplate;
}
