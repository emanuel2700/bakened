package pe.edu.uni.fiis.poo.ventaComida.dao;

//para los servicios Dao

import pe.edu.uni.fiis.poo.ventaComida.domain.Cliente;

public interface MyDao {
    Cliente registrarCliente(Cliente request);

}
