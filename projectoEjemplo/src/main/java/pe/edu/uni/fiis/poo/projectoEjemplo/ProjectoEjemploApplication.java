package pe.edu.uni.fiis.poo.projectoEjemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectoEjemploApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectoEjemploApplication.class, args);
	}

}
