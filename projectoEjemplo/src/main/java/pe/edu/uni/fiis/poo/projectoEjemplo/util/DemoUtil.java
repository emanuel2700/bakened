package pe.edu.uni.fiis.poo.projectoEjemplo.util;

import pe.edu.uni.fiis.poo.projectoEjemplo.domain.Persona;
import pe.edu.uni.fiis.poo.projectoEjemplo.dto.PersonaRequest;

public class DemoUtil {
    public static Persona mapToPersona(PersonaRequest request){
        Persona response = new Persona();
        response.setApellidoUsuario(request.getApellidoUsuario());
        response.setCodUsuario(request.getCodUsuario());
        response.setCondicion(request.getCondicion());
        response.setCorreo(request.getCorreo());
        response.setDni(request.getDni());
        response.setIdUsuario(request.getIdUsuario());
        response.setNombreUsuario(request.getNombreUsuario());
        response.setPassword(request.getPassword());
        response.setTipoUsuario(request.getTipoUsuario());
        response.setUsuarioCreacion(request.getUsuarioCreacion());
        return response;
    }
}
