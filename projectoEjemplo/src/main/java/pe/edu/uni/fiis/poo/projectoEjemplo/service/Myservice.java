package pe.edu.uni.fiis.poo.projectoEjemplo.service;

import pe.edu.uni.fiis.poo.projectoEjemplo.dto.PersonaRequest;
import pe.edu.uni.fiis.poo.projectoEjemplo.dto.PersonaResponse;

public interface Myservice {
    PersonaResponse registrarPersona (PersonaRequest request);
}
