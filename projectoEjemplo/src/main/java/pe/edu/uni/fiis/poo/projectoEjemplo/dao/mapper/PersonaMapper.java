package pe.edu.uni.fiis.poo.projectoEjemplo.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.edu.uni.fiis.poo.projectoEjemplo.domain.Persona;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonaMapper implements RowMapper {
    @Override
    public Persona mapRow(ResultSet resultSet, int i) throws SQLException {
        Persona response = new Persona();
        response.setIdUsuario(resultSet.getString("idUsuario"));
        response.setCodUsuario(resultSet.getString("codUsuario"));
        response.setPassword(resultSet.getString("password"));
        response.setDni(resultSet.getString("dni"));
        response.setNombreUsuario(resultSet.getString("nombreUsuario"));
        response.setApellidoUsuario(resultSet.getString("apellidoUsuario"));
        response.setCorreo(resultSet.getString("correo"));
        response.setTipoUsuario(resultSet.getString("tipoUsuario"));
        response.setCondicion(resultSet.getString("condicion"));
        response.setUsuarioCreacion(resultSet.getString("usuarioCreacion"));
        return response;
    }
}
