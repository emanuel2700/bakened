package pe.edu.uni.fiis.poo.projectoEjemplo.dao;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.poo.projectoEjemplo.dao.datasource.MyDatasource;
import pe.edu.uni.fiis.poo.projectoEjemplo.dao.mapper.PersonaMapper;
import pe.edu.uni.fiis.poo.projectoEjemplo.domain.Persona;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {
    public Persona generarIdPersona(Persona request) {
        String sql = " select 'U'||trim(to_char( " +
                "          to_number(substr(max(idUsuario),2,7),'9999999')+1" +
                "           ,'0000009')) idUsuario " +
                " null codUsuario, null password, null dni, null nombreUsuario, null apellidoUsuario, null correo, null tipoUsuario, null condicion, null usuarioCreacion" +
                " from persona";
        String persona = this.jdbcTemplate.queryForObject(sql, new PersonaMapper());
        request.setIdUsuario(persona.getIdUsuario());

        return request;
    }

    @Override
    public Persona registrarDatosPersona(Persona request) {
        return null;
    }

    @Override
    public Persona registrarPersona(Persona request) {
        return null;
    }
}
