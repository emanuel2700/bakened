package pe.edu.uni.fiis.poo.projectoEjemplo.dao;

import pe.edu.uni.fiis.poo.projectoEjemplo.domain.Persona;

public interface MyDao {
    Persona registrarPersona (Persona request);
    Persona registrarDatosPersona (Persona request);
    Persona generarIdPersona (Persona request);
}
