new Vue({
    el: '#acceso',
    data: {
        dni: '',
        nombre: '',
        celular: '',
        cod_confirmacion: '',
        comprador: '',
        repartidor: '',
        distrito: ''
    },
    methods: {
        crearUsuario: function(){
            var vm = this;
            var url = 'RegistrarUsuario';
            var data = {
                "dni": this.dni,
                "nombre": this.nombre,
                "celular": this.celular,
                "cod_confirmacion": this.cod_confirmacion,
                "comprador": this.comprador,
                "repartidor": this.repartidor,
                "distrito": this.distrito
            };
            fetch(url,{
                method: 'POST',
                body: JSON.stringify(data),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json()).catch(error => console.error('Error:', error))
                .then(response => {
                    console.log(response);
                })
            .catch(error => console.error('Error:', error))
        }
    }
});