package pe.edu.uni.fiis.poo.VentaOnline.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioRequest;
import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioResponse;
import pe.edu.uni.fiis.poo.VentaOnline.service.MyService;

@RestController
public class MyController {

    @Autowired
    private MyService myService;

    @PostMapping(value = "/RegistrarUsuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse registrarUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.registrarUsuario(request);
        return response;
    }
}
