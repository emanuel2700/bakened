package pe.edu.uni.fiis.poo.VentaOnline.dao;

import pe.edu.uni.fiis.poo.VentaOnline.domain.Usuario;

public interface MyDao {
    Usuario registrarUsuario(Usuario request);
}
