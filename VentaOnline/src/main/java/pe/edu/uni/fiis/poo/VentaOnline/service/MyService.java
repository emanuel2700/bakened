package pe.edu.uni.fiis.poo.VentaOnline.service;

import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioRequest;
import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioResponse;

public interface MyService {
    UsuarioResponse registrarUsuario (UsuarioRequest request);
}
