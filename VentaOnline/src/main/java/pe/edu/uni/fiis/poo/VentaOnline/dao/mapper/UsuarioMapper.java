package pe.edu.uni.fiis.poo.VentaOnline.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.edu.uni.fiis.poo.VentaOnline.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper implements RowMapper<Usuario> {

    public Usuario mapRow(ResultSet resultSet, int i) throws SQLException {
        Usuario response = new Usuario();
        response.setCelular(resultSet.getString("celular"));
        response.setCod_confirmacion(resultSet.getString("cod_confirmacion"));
        response.setComprador(resultSet.getString("comprador"));
        response.setDistrito(resultSet.getString("distrito"));
        response.setDni(resultSet.getString("dni"));
        response.setNombre(resultSet.getString("nombre"));
        response.setRepartidor(resultSet.getString("repartidor"));
        return response;
    }

}
