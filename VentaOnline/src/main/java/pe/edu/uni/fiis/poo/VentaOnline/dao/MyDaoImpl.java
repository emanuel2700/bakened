package pe.edu.uni.fiis.poo.VentaOnline.dao;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.poo.VentaOnline.dao.datasource.MyDatasource;
import pe.edu.uni.fiis.poo.VentaOnline.domain.Usuario;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao{
    @Override
    public Usuario registrarUsuario(Usuario request) {

       /* if (request.getCod_confirmacion() != "00000"){
            return null;
        }*/

        String sql = "insert into usuario (dni, " +
                "nombre, \n" +
                "celular, \n" +
                "cod_confirmacion, \n" +
                "comprador, \n" +
                "repartidor, \n" +
                "distrito)" +
                "values (?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(sql, new String[]{
                request.getDni(),
                request.getNombre(),
                request.getCelular(),
                request.getCod_confirmacion(),
                request.getComprador(),
                request.getRepartidor(),
                request.getDistrito()
        });

        return request;
    }
}
