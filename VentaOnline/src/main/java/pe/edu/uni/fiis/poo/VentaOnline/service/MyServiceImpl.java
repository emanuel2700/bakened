package pe.edu.uni.fiis.poo.VentaOnline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.uni.fiis.poo.VentaOnline.dao.MyDao;
import pe.edu.uni.fiis.poo.VentaOnline.domain.Usuario;
import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioRequest;
import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioResponse;
import pe.edu.uni.fiis.poo.VentaOnline.util.DemoUtil;

@Service
public class MyServiceImpl implements MyService{

    @Autowired
    private MyDao myDao;

    @Override
    public UsuarioResponse registrarUsuario(UsuarioRequest request) {
        Usuario usuario = this.myDao.registrarUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response = new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }
}
