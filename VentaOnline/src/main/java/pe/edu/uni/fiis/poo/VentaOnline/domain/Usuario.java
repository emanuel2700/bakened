package pe.edu.uni.fiis.poo.VentaOnline.domain;

public class Usuario {
    private String dni;
    private String nombre ;
    private String celular ;
    private String cod_confirmacion ;
    private String comprador ;
    private String repartidor;
    private String distrito ;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCod_confirmacion() {
        return cod_confirmacion;
    }

    public void setCod_confirmacion(String cod_confirmacion) {
        this.cod_confirmacion = cod_confirmacion;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getRepartidor() {
        return repartidor;
    }

    public void setRepartidor(String repartidor) {
        this.repartidor = repartidor;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }
}
