package pe.edu.uni.fiis.poo.VentaOnline.util;

import pe.edu.uni.fiis.poo.VentaOnline.domain.Usuario;
import pe.edu.uni.fiis.poo.VentaOnline.dto.Usuario.UsuarioRequest;

public class DemoUtil {
    public static Usuario mapToUsuario(UsuarioRequest request){
        Usuario response = new Usuario();
        response.setComprador(request.getComprador());
        response.setRepartidor(request.getRepartidor());
        response.setNombre(request.getNombre());
        response.setDni(request.getDni());
        response.setDistrito(request.getDistrito());
        response.setCod_confirmacion(request.getCod_confirmacion());
        response.setCelular(request.getCelular());
        return response;
    }
}
