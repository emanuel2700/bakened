package pe.edu.uni.fiis.ProyectoEjm.dao;

import pe.edu.uni.fiis.ProyectoEjm.domain.Persona;
import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;

public interface MyDao {
    public Usuario loginUsuario(Usuario request);
    Persona registrarPersona (Persona request);
    Persona registrarDatosPersona (Persona request);
    Persona generarIdPersona (Persona request);
}
