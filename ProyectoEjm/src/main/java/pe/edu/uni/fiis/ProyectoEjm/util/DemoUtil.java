package pe.edu.uni.fiis.ProyectoEjm.util;

import pe.edu.uni.fiis.ProyectoEjm.domain.Persona;
import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;
import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaRequest;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioRequestWeb;

public class DemoUtil {
    public static Usuario mapToUsuario(UsuarioRequestWeb requestWeb){
        Usuario responseweb = new Usuario();
        responseweb.setCredencial(requestWeb.getCredencial());
        responseweb.setCod_usuario(requestWeb.getCodUsuario());
        return responseweb;
    }

    public static Persona mapToPersona(PersonaRequest request){
        Persona response = new Persona();
        response.setApellidoUsuario(request.getApellidoUsuario());
        response.setCodUsuario(request.getCodUsuario());
        response.setCondicion(request.getCondicion());
        response.setCorreo(request.getCorreo());
        response.setDni(request.getDni());
        response.setIdUsuario(request.getIdUsuario());
        response.setNombreUsuario(request.getNombreUsuario());
        response.setPassword(request.getPassword());
        response.setTipoUsuario(request.getTipoUsuario());
        response.setUsuarioCreacion(request.getUsuarioCreacion());
        return response;
    }
}
