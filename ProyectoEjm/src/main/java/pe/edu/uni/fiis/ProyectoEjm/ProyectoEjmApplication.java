package pe.edu.uni.fiis.ProyectoEjm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoEjmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoEjmApplication.class, args);
	}

}
