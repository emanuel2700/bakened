package pe.edu.uni.fiis.ProyectoEjm.dao;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.ProyectoEjm.dao.datasource.MyDatasource;
import pe.edu.uni.fiis.ProyectoEjm.dao.mapper.PersonaMapper;
import pe.edu.uni.fiis.ProyectoEjm.dao.mapper.UsuarioMapper;
import pe.edu.uni.fiis.ProyectoEjm.domain.Persona;
import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {

    public Usuario loginUsuario(Usuario request){

        Usuario usuario = null;

        try {
            usuario = (Usuario) this.jdbcTemplate.queryForObject(
                    "select cod_usuario," +
                            "credencial," +
                            "nombres," +
                            "apellidos," +
                            "estado from usuario "+
                            "where cod_usuario = ? "+
                            "and credencial = ? ", new String[]{
                            request.getCod_usuario(), request.getCredencial()}, new UsuarioMapper());

        }catch (Exception ex){
            return usuario;
        }
        return usuario;
    }

    @Override
    public Persona registrarPersona(Persona request) {
        request = generarIdPersona(request);
        return registrarDatosPersona(request);
    }

    @Override
    public Persona generarIdPersona(Persona request) {
        String sql = " select 'U'||trim(to_char( " +
                "          to_number(substr(max(cod_usuario),2,7),'9999999')+1" +
                "           ,'0000009')) idUsuario, " +
                " null codUsuario, null password, null dni, null nombreUsuario, null apellidoUsuario, null correo, null tipoUsuario, null condicion, null usuarioCreacion" +
                " from persona";
        Persona persona = this.jdbcTemplate.queryForObject(sql, new PersonaMapper());
        request.setIdUsuario(persona.getIdUsuario());

        return request;
        //idUsuario, codUsuario, password,  dni, nombreUsuario, apellidoUsuario, correo, tipoUsuario, condicion, usuarioCreacion;
    }
}
