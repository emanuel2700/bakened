package pe.edu.uni.fiis.ProyectoEjm.dto;

public class UsuarioRequestWeb {
    private String codUsuario;
    private String credencial;

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getCredencial() {
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }
}
