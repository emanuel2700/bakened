package pe.edu.uni.fiis.ProyectoEjm.dto;

import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;

public class UsuarioResponseWeb {
    private String error;
    private Usuario usuario;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
