package pe.edu.uni.fiis.ProyectoEjm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;
import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaRequest;
import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaResponse;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioRequestWeb;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioResponseWeb;
import pe.edu.uni.fiis.ProyectoEjm.service.MyService;

// aquí van a ir tus controladores
@RestController // importante
public class MyController {

    @Autowired
    private MyService myService;

    @PostMapping(value="/usuarios",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Usuario getUser(){
        return new Usuario();
    }

    @PostMapping(value="/usuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponseWeb loginUser(@RequestBody UsuarioRequestWeb requestWeb){
        return this.myService.loginUsuario(requestWeb);
    }

    @PostMapping(value = "/crearPersona",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public PersonaResponse registrarPersona(@RequestBody PersonaRequest request){
        PersonaResponse response = this.myService.registrarPersona(request);
        return response;
    }

}
