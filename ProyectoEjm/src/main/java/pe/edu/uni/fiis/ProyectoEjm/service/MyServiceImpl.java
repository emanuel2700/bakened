package pe.edu.uni.fiis.ProyectoEjm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.uni.fiis.ProyectoEjm.dao.MyDao;
import pe.edu.uni.fiis.ProyectoEjm.domain.Persona;
import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;
import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaRequest;
import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaResponse;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioRequestWeb;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioResponseWeb;
import pe.edu.uni.fiis.ProyectoEjm.util.DemoUtil;

@Service // define a la la clase como la que da servicios
public class MyServiceImpl implements MyService{

    @Autowired
    private MyDao myDao;

    @Override
    public UsuarioResponseWeb loginUsuario(UsuarioRequestWeb requestWeb){
        Usuario usuario = this.myDao.loginUsuario(DemoUtil.mapToUsuario(requestWeb));
        UsuarioResponseWeb responseWeb = new UsuarioResponseWeb();
        responseWeb.setUsuario(usuario);
        return responseWeb;
    }

    @Override
    public PersonaResponse registrarPersona(PersonaRequest request) {
        Persona persona = this.myDao.registrarPersona(DemoUtil.mapToPersona(request));
        PersonaResponse response = new PersonaResponse();
        response.setPersona(persona);
        return response;
    }
}
