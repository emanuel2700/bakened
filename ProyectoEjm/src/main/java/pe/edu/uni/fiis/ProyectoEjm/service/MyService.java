package pe.edu.uni.fiis.ProyectoEjm.service;

import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaRequest;
import pe.edu.uni.fiis.ProyectoEjm.dto.PersonaResponse;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioRequestWeb;
import pe.edu.uni.fiis.ProyectoEjm.dto.UsuarioResponseWeb;

public interface MyService {
    UsuarioResponseWeb loginUsuario(UsuarioRequestWeb requestWeb);
    PersonaResponse registrarPersona (PersonaRequest request);
}
