package pe.edu.uni.fiis.ProyectoEjm.dto;

import pe.edu.uni.fiis.ProyectoEjm.domain.Persona;

public class PersonaResponse {
    private String error;
    private Persona persona;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
