package pe.edu.uni.fiis.ProyectoEjm.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.edu.uni.fiis.ProyectoEjm.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        Usuario response = new Usuario();
        response.setCod_usuario(resultSet.getString("cod_usuario"));
        response.setCredencial(resultSet.getString("credencial"));
        response.setNombres(resultSet.getString("nombres"));
        response.setApellidos(resultSet.getString("apellidos"));
        response.setEstado((resultSet.getString("estado")));
        return response;
    }
}
